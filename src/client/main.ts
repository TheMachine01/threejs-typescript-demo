import { createApp } from "vue";
import { IonicVue } from "@ionic/vue";
// import router from './router/index.js' 
import App from "./App.vue";

import "./assets/main.css";

createApp(App).use(IonicVue)
// .use(router)
.mount("#app");
