import { createRouter, createWebHistory } from "@ionic/vue-router";

const routes = [];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
